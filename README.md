NOTAS SOBRE O Git (incluindo servidores remotos)

Criação do projeto:

	$ git init

Adição de arquivo para ser monitorado:

	$ git add arquivo

Obter informações sobre o projeto:

	$ git status

Criação de uma nova versão:

	$ git commit -m "Descrição da nova versão"

Para listar as versões:

	$ git log

Para alterar a versão atual:

	$ git checkout versão

...onde 'versão' é o código mostrado por:

	$ git log --oneline --decorate --all

Para enviar alterações para um servidor remoto:

	$ git push origin master

Para baixar alterações de um servidor remoto:

	$ git pull origin master

...ou então:

	$ git pull origin

(Note que origin e master são nomes genéricos que podem ser alterados (pelo que entendi))
